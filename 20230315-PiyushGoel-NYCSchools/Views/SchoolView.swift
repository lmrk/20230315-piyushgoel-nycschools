//
//  SchoolView.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import SwiftUI

//MARK:- Declare SchoolView

@MainActor
struct SchoolView: View {
    
    @StateObject private var viewModel = SchoolViewModel()
    @State private var isViewLoaded = false
    
    var body: some View {
        NavigationView {
            List {
                if viewModel.isLoading {
                    ProgressView()
                } else {
                    ForEach(viewModel.schoolDetail, id: \.self) {
                        item in
                        NavigationLink {
                            DetailView(viewModel: viewModel, dbnValue: item.dbNumber)
                        } label: {
                            Text(item.schoolName)
                                .foregroundColor(.white)
                        }
                    }
                    .listRowBackground(Color.purple)
                    .listRowSeparatorTint(.white)
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text(Constants.homeViewTitle)
                        .font(.title.bold())
                        .foregroundColor(.purple)
                        .accessibilityAddTraits(.isHeader)
                }
            }
            .onAppear {
                Task {
                    if !isViewLoaded {
                        _ =  await viewModel.getSchoolDetail()
                        isViewLoaded = true
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolView()
    }
}
