//
//  SchoolViewModel.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import Foundation

//MARK:- SchoolViewModel

@MainActor
class SchoolViewModel: ObservableObject {
    
    @Published var schoolDetail: [SchoolDetail] = []
    @Published var satScoreDetail: [SATScoreDetail] = []
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    
    //MARK:- Function to  getSchoolDetail
    func getSchoolDetail() async -> [SchoolDetail]? {
        do {
            isLoading = true
            schoolDetail =  try await HTTPUtility.shared.performSchoolOperation(url: Service.schoolURL, queryItem: "", response: [SchoolDetail].self)
            isLoading = false
        } catch let error {
            print(error)
        }
        return schoolDetail.isEmpty ? [] : schoolDetail
    }
    
    //MARK:- Function to  getSATScoreDetail
    func getSATScoreDetail(queryValue: String) async -> [SATScoreDetail]? {
        do {
            satScoreDetail = try await HTTPUtility.shared.performSchoolOperation(url: Service.SATScoreURL, queryItem: queryValue, response: [SATScoreDetail].self)
        } catch let error {
            print(error)
        }
        if satScoreDetail.isEmpty {
            showAlert = true
        }
        return satScoreDetail.isEmpty ? [] : satScoreDetail
    }
}
