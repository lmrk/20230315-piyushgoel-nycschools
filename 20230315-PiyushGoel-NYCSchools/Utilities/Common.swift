//
//  Common.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import Foundation

//MARK:- Declare Service URL

struct Service {
    static let schoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let SATScoreURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}

//MARK:- Declare Constants

struct Constants {
    static let homeViewTitle = "List of NYC High Schools"
    static let detailViewTitle = "SAT Scores"
    static let ReadingScore = "Reading Score:-"
    static let WritingScore = "Writing Score:-"
    static let MathScore = "Math Score:-"
    static let alertMessage = "No records Found, Please try again..."
    static let alertTitle = "Alert !!!"
    static let okMessage = "Press ok here"
}
