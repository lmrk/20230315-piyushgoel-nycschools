//
//  SchoolModel.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import Foundation

//MARK:- SchoolDetail

struct SchoolDetail: Decodable, Hashable {
    let dbNumber: String
    let schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbNumber = "dbn"
        case schoolName = "school_name"
    }
}

//MARK:- SatScoreDetails

struct SATScoreDetail: Decodable,Hashable {
    
    let mathScore: String
    let readingScore: String
    let writingScore: String
    
    enum CodingKeys: String, CodingKey {
        case mathScore = "sat_math_avg_score"
        case readingScore = "sat_critical_reading_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
